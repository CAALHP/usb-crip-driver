﻿using System;
using caalhp.IcePluginAdapters;

namespace UsbCripDriver
{
    public class Program
    {
        private static DeviceDriverAdapter _adapter;
        private static UsbCripDriver _implementation;

        static void Main(string[] args)
        {
            const string endpoint = "localhost";
            try
            {
                _implementation = new UsbCripDriver();
                _adapter = new DeviceDriverAdapter(endpoint, _implementation);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Press <ENTER> to exit program.");
            Console.ReadLine();
        }
    }
}